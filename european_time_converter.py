import datetime

def convert(us_time):
    datetime_us = datetime.datetime.strptime(us_time, '%Y-%m-%dT%H:%M:%SZ')
    return (datetime_us + datetime.timedelta(hours=-4)).strftime("%m/%d/%Y %I:%M:%S %p")

sample_date_string = "2019-07-01T00:00:00Z"

lambda_convert = lambda x: (datetime.datetime.strptime(x, '%Y-%m-%dT%H:%M:%SZ') + datetime.timedelta(hours=-4))\
    .strftime("%m/%d/%Y %I:%M:%S %p")
print(lambda_convert(sample_date_string))